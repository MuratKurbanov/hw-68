import axios from '../axios-todo';

export const GET_ALL_TASKS = 'GET_ALL_TASKS';

export const getAllTasksSuccess = data => {
    return {
        type: GET_ALL_TASKS, data
    }
};

export const getTasks = () => {
    return dispatch => {
        axios.get('./task.json').then((tasks) => {
            dispatch(getAllTasksSuccess(tasks.data))
        });
    }
};

export const addTask = task => {
    return dispatch => {
        axios.post('./task.json', task).then(() => {
            dispatch(getAllTasksSuccess())
        })
    }
};

