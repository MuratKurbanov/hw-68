 import {
     GET_ALL_TASKS
 } from "./action";

 const initialState = {
     tasks: null,
 };

 const reducer = (state = initialState, action) => {

     switch (action.type) {
         case GET_ALL_TASKS:
             return {
                 ...state,
                 tasks: action.data
             };
         default: return state
     }
 };

 export default reducer;