import React, {Component} from 'react';
import './TaskForm.css'
import {addTask, getTasks} from "../../store/action";
import connect from "react-redux/es/connect/connect";

class TaskForm extends Component {
    state = {
        text: ''
    };

    valueChange = event => {
        this.setState({
            text: event.target.value
        })
    };

    sendHandler = () => {
        const task = {text: this.state.text};
        this.props.sendTask(task)
    };

    componentDidMount() {
        this.props.getTasks()
    }

    render() {
        let tasks = [];
        for(let key in this.props.tasks) {
            tasks.push(<p key={key} className='text'>{this.props.tasks[key].text}</p>)
        }
        return (
            <div className='TaskForm'>
                <div>
                    <input value={this.state.text} type="text" placeholder='Add new Task' onChange={this.valueChange}/>
                    <button className='button' onClick={this.sendHandler}>Send</button>
                </div>
                <div>
                    {tasks}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        tasks: state.tasks
    }
};

const mapDispatchToProps = dispatch => {
    return {
        sendTask: (task) => dispatch(addTask(task)),
        getTasks: () => dispatch(getTasks())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(TaskForm);