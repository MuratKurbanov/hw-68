import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://todo-list-706f1.firebaseio.com/'
});

export default instance;